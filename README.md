Main paper
====================

The main paper "Non-Isotropic Persistent Homology: Leveraging the Metric Dependency of PH", Vincent P. Grande and Michael T. Schaub, accepted at LoG 2023, can be found under <https://arxiv.org/abs/2310.16437>.

## Funding

VPG and MTS acknowledge funding by the German Research Council (DFG) within Research Training Group 2236 (UnRAVeL).
MTS acknowledges partial funding by the Ministry of Culture and Science (MKW) of the German State of North Rhine-Westphalia ("NRW Rückkehrprogramm") and the European Union (ERC, HIGH-HOPeS, 101039827). Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union or the European Research Council Executive Agency. Neither the European Union nor the granting authority can be held responsible for them.
